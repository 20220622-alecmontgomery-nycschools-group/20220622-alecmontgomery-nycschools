//
//  AppStrings.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/22/22.
//

import Foundation

/// Would use localized strings in a prod app
struct AppStrings {
    static let newline = "\n"
    static let blank = "-"
    
    struct SchoolList {
        static let refresh = "Refresh"
        static let schoolListSuffix = " NYC Schools"
        static let noSchools = "No Schools"
        static let errorState = "Error fetching data. Try refreshing."
    }

    struct SchoolDetailView {
        static let gradesServedPrefix = "Grades Served: "
        static let numberOfStudentsPrefix = "Number of Students: "
        static let satScoresTitlePrefix = "Average SAT Scores for "
        static let satCriticalReadingPrefix = "Critical Reading: "
        static let satMathPrefix = "Math: "
        static let satWritingPrefix = "Writing: "
    }
}
