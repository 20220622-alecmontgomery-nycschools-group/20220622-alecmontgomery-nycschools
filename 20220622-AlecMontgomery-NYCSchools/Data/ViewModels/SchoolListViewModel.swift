//
//  SchoolListViewModel.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/22/22.
//

import Foundation
import Combine
import SwiftUI

class SchoolListViewModel: ObservableObject {
    @Published var isLoading = false
    @Published var selectedSchool: SchoolDetailViewModel?
    
    private(set) var error: Error?
    private(set) var schoolList = CurrentValueSubject<[SchoolDetailViewModel], Never>([SchoolDetailViewModel]())
    
    private let schoolFetcher: SchoolFetcher
    private var anyCancellable: AnyCancellable?
    
    init(schoolFetcher: SchoolFetcher = SchoolFetcherImpl()) {
        self.schoolFetcher = schoolFetcher
        anyCancellable = self.schoolFetcher.schoolList.sink(receiveValue: { model in
            self.isLoading = false
            self.schoolList.send(model)
            self.objectWillChange.send()
        })
        
        self.getAllSchools()
    }
    
    func getAllSchools(_ updateQueue: DispatchQueue = DispatchQueue.main) {
        self.isLoading = true
        
        self.schoolFetcher.getSchoolDetailViewModels(updateQueue)
    }
    
    var listTitle: String {
        School.year + AppStrings.SchoolList.schoolListSuffix
    }
    
    private var noSchools: Bool {
        self.schoolList.value.isEmpty
    }
    
    private var showErrorView: Bool {
        error != nil
    }
    
    var showEmptyStateView: Bool {
        noSchools || showErrorView
    }
    
    var emptyStateText: String {
        showErrorView ? AppStrings.SchoolList.errorState : AppStrings.SchoolList.noSchools
    }
}
