//
//  SchoolDetailViewModel.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/22/22.
//

import Foundation
import UIKit
import SwiftUI

@objc class SchoolDetailViewModel: NSObject, Identifiable {
    let id = UUID()
    let school: School
    let satScores: SATResults
    
    init(school: School, satScores: SATResults) {
        self.school = school
        self.satScores = satScores
    }
    
    @objc var schoolName: String {
        return school.name ?? AppStrings.blank
    }
    
    @objc var gradesServed: String {
        let gradesV = school.gradesServed ?? AppStrings.blank
        return AppStrings.SchoolDetailView.gradesServedPrefix + gradesV
    }
    
    @objc var totalStudents: String {
        let totalStudentsV = school.totalStudents ?? AppStrings.blank
        return AppStrings.SchoolDetailView.numberOfStudentsPrefix + totalStudentsV
    }
    
    @objc lazy var borough: String = {
        return school.borough?.capitalized ?? AppStrings.blank
    }()
    
    @objc lazy var satScoresDescription: String = {
        let text = SATResults.year
        
        func transformSatScoreValue(_ score: String?) -> String {
            return stringIsNumber(score) ? (score ?? "") : AppStrings.blank
        }
        
        var readingScoreText = transformSatScoreValue(satScores.criticalReadingAvgScore)
        var mathScoreText = transformSatScoreValue(satScores.mathAvgScore)
        var writingScoreText = transformSatScoreValue(satScores.writingAvgScore)
        
        let line0 = AppStrings.SchoolDetailView.satScoresTitlePrefix + text
        let line1 = AppStrings.SchoolDetailView.satCriticalReadingPrefix + readingScoreText
        let line2 = AppStrings.SchoolDetailView.satMathPrefix + mathScoreText
        let line3 = AppStrings.SchoolDetailView.satWritingPrefix + writingScoreText
        
        return [line0, line1, line2, line3].joined(separator: AppStrings.newline)
    }()
    
    /// modified from: https://stackoverflow.com/a/43365841/744464
    @objc static var randomBgColor: UIColor {
        return UIColor(
            red: .random(in: 0.93...1),
            green: .random(in: 0.93...1),
            blue: .random(in: 0.98...1),
            alpha: 1.0
        )
    }
    
    // modified from: https://stackoverflow.com/a/38481180/744464
    private func stringIsNumber(_ string: String?) -> Bool {
        guard let string = string else {
            return false
        }
        
        return !string.isEmpty && string.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}
