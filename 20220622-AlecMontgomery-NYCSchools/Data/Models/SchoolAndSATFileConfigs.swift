//
//  SchoolAndSATFileConfigs.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/24/22.
//

import Foundation

enum SchoolModelCSVConfig: String, CSVFileConfig {
    typealias ModelType = School
    
    case schoolID
    case name
    case borough
    case gradesServed
    case totalStudents
    
    var keyName: String {
        return self.rawValue
    }
    
    static var uniqueIdentifierColumn: Int {
        return SchoolModelCSVConfig.schoolID.columnNumber
    }
    
    var columnNumber: Int {
        switch self {
        case .schoolID:
            return 0
        case .name:
            return 1
        case .borough:
            return 461
        case .gradesServed:
            return 25
        case .totalStudents:
            return 27
        }
    }
    
    static var datasetInfo: CSVDatasetInfo {
        CSVDatasetInfo(datasetYear: "2017", filename: "2017_DOE_High_School_Directory")
    }
}

enum SATModelCSVConfig: String, CSVFileConfig {
    typealias ModelType = SATResults
    
    case schoolID
    case criticalReadingAvgScore
    case mathAvgScore
    case writingAvgScore
    
    var keyName: String {
        return self.rawValue
    }
    
    static var uniqueIdentifierColumn: Int {
        return SchoolModelCSVConfig.schoolID.columnNumber
    }
    
    static var datasetInfo: CSVDatasetInfo {
        CSVDatasetInfo(datasetYear: "2012", filename: "2012_SAT_Results")
    }
    
    var columnNumber: Int {
        switch self {
        case .schoolID:
            return 0
        case .criticalReadingAvgScore:
            return 3
        case .mathAvgScore:
            return 4
        case .writingAvgScore:
            return 5
        }
    }
}
