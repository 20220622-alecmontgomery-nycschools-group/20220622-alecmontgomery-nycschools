//
//  School.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/22/22.
//

import Foundation

struct School: Codable {
    let name: String?
    let schoolID: String?
    let borough: String?
    let gradesServed: String?
    let totalStudents: String?
    
    // would inject in other scenarios with multiple years
    static var year: String {
        SchoolModelCSVConfig.datasetInfo.datasetYear
    }
}

struct SATResults: Codable {
    let schoolID: String?
    let criticalReadingAvgScore: String?
    let writingAvgScore: String?
    let mathAvgScore: String?
    
    // would inject in other scenarios with multiple years
    static var year: String {
        SATModelCSVConfig.datasetInfo.datasetYear
    }
}
