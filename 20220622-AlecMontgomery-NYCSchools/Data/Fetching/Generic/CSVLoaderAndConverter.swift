//
//  CSVLoaderAndConverter.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/24/22.
//

import Foundation
import CSV

/// Loads a CSV file specified by a `CSVFileConfig` and converts it to the specified Codable type
/// Would setup dependency injection with more time 
class CSVLoaderAndConverter {
    
    typealias IdentifierToCodableDictionary = [String: Codable]
    
    func filePathForName(name: String) -> String? {
        return Bundle.main.path(forResource: name, ofType: "csv")
    }
    
    /// Takes a CSV file, and returns an array of models keyed by a unique ID
    func loadAndConvertCSVFile<T: CSVFileConfig>(config: T.Type) -> IdentifierToCodableDictionary? {
        guard let path = self.filePathForName(name: T.datasetInfo.filename) else {
            print("No path")
            return nil
        }
        guard let stream = InputStream(fileAtPath: path) else {
            return nil
        }
        
        let csv = try! CSVReader(stream: stream)
        
        var codableItems = IdentifierToCodableDictionary()
    
        let encoder = JSONEncoder()
        let decoder = JSONDecoder()
    
        while let row = csv.next() {
            
            var keyValues = [String: String]()
            
            for schoolItemColumn in T.allCases {
                if schoolItemColumn.columnNumber < row.count {
                    let valueAtColumn = row[schoolItemColumn.columnNumber]
                    keyValues[schoolItemColumn.keyName] = valueAtColumn
                }
            }
            
            /// convert dictionary to Codable object
            do {
                let encodedDictionary = try encoder.encode(keyValues)
                let decoded = try decoder.decode(T.ModelType.self, from: encodedDictionary)
                if T.uniqueIdentifierColumn < row.count {
                    let uniqueID = row[T.uniqueIdentifierColumn]
                    codableItems[uniqueID] = decoded
                }
            } catch {
                print("Error encoding or decoding: ", error)
            }
        }
        
        return codableItems
    }
}
