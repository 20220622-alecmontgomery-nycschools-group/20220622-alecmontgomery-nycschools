//
//  CSVColumnMapping.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/24/22.
//

import Foundation

struct CSVDatasetInfo {
    let datasetYear: String
    let filename: String
}

/// Specifies the column information and Codable model to parse rows to
protocol CSVFileConfig: CaseIterable {
    associatedtype ModelType: Codable

    static var datasetInfo: CSVDatasetInfo { get }
    static var uniqueIdentifierColumn: Int { get }
    
    var columnNumber: Int { get }
    var keyName: String { get }
}
