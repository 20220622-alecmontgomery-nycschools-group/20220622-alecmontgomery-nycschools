//
//  SchoolFetcher.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/22/22.
//

import Foundation
import Combine

protocol SchoolFetcher {
    func getSchoolDetailViewModels(_ updateQueue: DispatchQueue)
    var schoolList: CurrentValueSubject<[SchoolDetailViewModel], Never> { get }
}

/// Gets the school list
class SchoolFetcherImpl: SchoolFetcher {
    
    private let persistenceController: SchoolPersistenceController
    private(set) var schoolList = CurrentValueSubject<[SchoolDetailViewModel], Never>([SchoolDetailViewModel]())
    
    init(persistenceController: SchoolPersistenceController = SchoolPersistenceController()) {
        self.persistenceController = persistenceController
    }
    
    func getSchoolDetailViewModels(_ updateQueue: DispatchQueue = DispatchQueue.main) {
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            guard let models = self?.persistenceController.getAllSchoolDetailViewModels() else {
                self?.schoolList.send([])
                return
            }
            updateQueue.async {
                self?.schoolList.send(models)
            }
        }
    }
}

class SchoolPersistenceController: CSVLoaderAndConverter {
    
    private let debug = false
    
    func getAllSchoolDetailViewModels() -> [SchoolDetailViewModel] {
        let allSchools = getAllSchools()
        let allSatScores = getAllSatScores()
     
        var detailModels = [SchoolDetailViewModel]()
        allSchools?.forEach({ key, value in
            if let correspondingSatScore = allSatScores?[key] as? SATResults,
               let school = value as? School {
                detailModels.append(SchoolDetailViewModel(school: school, satScores: correspondingSatScore))
            }
        })
        
        return detailModels
    }
    
    private func getAllSchools() -> IdentifierToCodableDictionary? {
        let items = self.loadAndConvertCSVFile(config: SchoolModelCSVConfig.self)
    
        if debug {
            if let values = items?.values {
                for school in values {
                    if let school = school as? School {
                        print("school.name = \(school.name ?? "") ... \(school.totalStudents ?? "")")
                    }
                }
            }
        }
        
        return items
    }
    
    private func getAllSatScores() -> IdentifierToCodableDictionary? {
        let items = self.loadAndConvertCSVFile(config: SATModelCSVConfig.self)
        
        if debug {
            if let values = items?.values {
                for satResults in values {
                    if let satResults = satResults as? SATResults {
                        print("satResults.schoolId = \(satResults.schoolID ?? "") ... \(satResults.criticalReadingAvgScore ?? "")")
                    }
                }
            }
        }
        
        return items
    }
}
