//
//  SchoolDetailView.h
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/22/22.
//

#import <UIKit/UIKit.h>

@class SchoolDetailViewModel;

NS_ASSUME_NONNULL_BEGIN

@interface SchoolDetailView : UIView

- (id)initWithSchool:(SchoolDetailViewModel *)model;

@end

NS_ASSUME_NONNULL_END
