//
//  SchoolDetailViewRepresentable.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/24/22.
//

import Foundation
import SwiftUI

struct SchoolDetailViewRepresentable: UIViewRepresentable {
    let detailView: SchoolDetailView
    
    init(_ viewModel: SchoolDetailViewModel) {
        detailView = SchoolDetailView(school: viewModel)
    }
    
    func makeUIView(context: Context) -> some UIView {
        return detailView
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) { }
}
