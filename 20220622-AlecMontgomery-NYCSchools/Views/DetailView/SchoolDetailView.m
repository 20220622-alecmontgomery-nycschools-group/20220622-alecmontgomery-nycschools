//
//  SchoolDetailView.m
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/22/22.
//

#import "SchoolDetailView.h"
#include "_0220622_AlecMontgomery_NYCSchools-Swift.h"

@implementation SchoolDetailView

const CGFloat labelSpacing = 10;

- (id)initWithSchool:(SchoolDetailViewModel *)model {
    self = [super initWithFrame: CGRectZero];
    if (self) { 
        [self createView: model];
    }
    return self;
}

- (void)createView:(SchoolDetailViewModel *)detailViewModel {
    if (detailViewModel == NULL) {
        return;
    }
    
    NSArray *labels = [self labelsFromDetailViewModel: detailViewModel];
    for (UILabel *label in labels) {
        [label setTranslatesAutoresizingMaskIntoConstraints: false];
        [label setNumberOfLines: 0];
        [label setContentCompressionResistancePriority: UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    }
    
    const UIEdgeInsets edgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    
    UIStackView *vertStackView = [[UIStackView alloc] initWithArrangedSubviews: labels];
    [vertStackView setAxis: UILayoutConstraintAxisVertical];
    [vertStackView setSpacing: labelSpacing];
    [vertStackView setDistribution: UIStackViewDistributionEqualSpacing];
    
    [vertStackView setTranslatesAutoresizingMaskIntoConstraints: false];
    [self addSubview: vertStackView];
    
    [[vertStackView.topAnchor constraintEqualToAnchor: self.safeAreaLayoutGuide.topAnchor constant: edgeInsets.top] setActive:true];
    [[vertStackView.leadingAnchor constraintEqualToAnchor: self.safeAreaLayoutGuide.leadingAnchor constant: edgeInsets.left] setActive: true];
    NSLayoutConstraint *constraint = [vertStackView.trailingAnchor constraintEqualToAnchor: self.safeAreaLayoutGuide.trailingAnchor constant: -edgeInsets.right];
    constraint.priority = UILayoutPriorityDefaultHigh;
    [constraint setActive: true];
    
    [vertStackView sizeToFit];
    
    [self layoutSubviews];
    [self setBackgroundColor: SchoolDetailViewModel.randomBgColor];
}

- (NSArray *)labelsFromDetailViewModel:(SchoolDetailViewModel *)detailViewModel {
    UILabel *label1 = [UILabel new];
    label1.text = detailViewModel.schoolName;
    [label1 setFont:[UIFont preferredFontForTextStyle: UIFontTextStyleTitle1]];
    [label1 setTextColor: [UIColor blackColor]];
    
    UILabel *label2 = [UILabel new];
    label2.text = detailViewModel.borough;
    [label2 setFont:[UIFont preferredFontForTextStyle: UIFontTextStyleTitle2]];

    UILabel *label3 = [UILabel new];
    label3.text = detailViewModel.gradesServed;
    [label3 setFont:[UIFont preferredFontForTextStyle: UIFontTextStyleHeadline]];
    
    UILabel *label4 = [UILabel new];
    label4.text = detailViewModel.totalStudents;
    [label4 setFont:[UIFont preferredFontForTextStyle: UIFontTextStyleHeadline]];
    
    UILabel *label5 = [UILabel new];
    label5.text = detailViewModel.satScoresDescription;
    [label5 setFont:[UIFont preferredFontForTextStyle: UIFontTextStyleHeadline]];
    
    return @[label1, label2, label3, label4, label5];
}

@end
