//
//  ContentView.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/22/22.
//

import SwiftUI

struct SchoolListView: View {
    @ObservedObject var viewModel: SchoolListViewModel
    @State private var listOpacity: Double = 0
    
    init() {
        self.viewModel = SchoolListViewModel()
    }
    
    var body: some View {
        NavigationView {
            Group {
                if viewModel.isLoading {
                    ProgressView()
                        .onAppear(perform: {
                            withAnimation {
                                listOpacity = 0
                            }
                        })
                } else if viewModel.showEmptyStateView {
                    Text(viewModel.emptyStateText)
                } else {
                    List {
                        ForEach(self.viewModel.schoolList.value) { school in
                            ZStack {
                                SchoolCellView(viewModel: school)
                                    .background(Color(SchoolDetailViewModel.randomBgColor).edgesIgnoringSafeArea(.all))
                                NavigationLink(destination:  SchoolDetailViewRepresentable(school)) {
                                    EmptyView()
                                }
                                .buttonStyle(PlainButtonStyle()).opacity(0)
                            }
                            .listRowInsets(EdgeInsets())
                        }
                    }
                    .refreshable {
                        viewModel.getAllSchools()
                    }
                    .listStyle(.plain)
                    .opacity(listOpacity)
                    .onAppear(perform: {
                        withAnimation {
                            listOpacity = 1
                        }
                    })
                }
            }
            .navigationTitle(viewModel.listTitle)
            .navigationBarTitleDisplayMode(.inline)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct SchoolCellView: View {
    let viewModel: SchoolDetailViewModel
    let padding: CGFloat = 15
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text(viewModel.schoolName)
                .font(.system(.headline))
            
            HStack {
                Text(viewModel.borough)
                    .font(.system(.body))
                    .fixedSize(horizontal: false, vertical: true)
                Spacer()
            }
        }
        .padding(.all, padding)
        .frame(maxWidth: .infinity, maxHeight: .greatestFiniteMagnitude)
    }
}
