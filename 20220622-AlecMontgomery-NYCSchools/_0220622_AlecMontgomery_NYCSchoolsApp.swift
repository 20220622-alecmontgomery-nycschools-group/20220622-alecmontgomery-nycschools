//
//  _0220622_AlecMontgomery_NYCSchoolsApp.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/22/22.
//

import SwiftUI

@main
struct _0220622_AlecMontgomery_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolListView()
        }
    }
}
