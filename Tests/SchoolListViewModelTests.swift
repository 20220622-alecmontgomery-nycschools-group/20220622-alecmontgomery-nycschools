//
//  SchoolListViewModelTests.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/25/22.
//

import Foundation
import Combine
import XCTest

@testable import _0220622_AlecMontgomery_NYCSchools

class SchoolListViewModelTests: XCTestCase {
    
    func testGetAllSchools() {
        let model = SchoolListViewModel(schoolFetcher: SchoolFetcherMock())
        
        let queue = DispatchQueue(label: "EmployeeListTest")
        model.getAllSchools(queue)
        XCTAssertTrue(model.isLoading)
        queue.sync { }
        
        XCTAssertEqual(model.schoolList.value.count, 2)
    }
}

class SchoolFetcherMock: SchoolFetcher {
    var schoolList = CurrentValueSubject<[SchoolDetailViewModel], Never>([SchoolDetailViewModel]())
    
    func getSchoolDetailViewModels(_ updateQueue: DispatchQueue) {
        let school1 = School(name: "School1", schoolID: "SchoolId", borough: "Brooklyn", gradesServed: "9-12", totalStudents: "100")
        let satScore = SATResults(schoolID: "SchoolId", criticalReadingAvgScore: "123", writingAvgScore: "200", mathAvgScore: "300")
        
        let model = SchoolDetailViewModel(school: school1, satScores: satScore)

        let school2 = School(name: "School2", schoolID: "SchoolId2", borough: "Manhattan", gradesServed: "9-12", totalStudents: "200")
        let satScore2 = SATResults(schoolID: "SchoolId", criticalReadingAvgScore: "123", writingAvgScore: "200", mathAvgScore: "300")
        
        let model2 = SchoolDetailViewModel(school: school2, satScores: satScore2)

        updateQueue.async {
            self.schoolList.send([model, model2])
        }
    }
}
