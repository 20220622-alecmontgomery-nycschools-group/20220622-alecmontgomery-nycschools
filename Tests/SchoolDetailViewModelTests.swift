//
//  SchoolDetailViewModelTests.swift
//  20220622-AlecMontgomery-NYCSchools
//
//  Created by Alec Montgomery on 6/25/22.
//

import Foundation
import XCTest

@testable import _0220622_AlecMontgomery_NYCSchools

class SchoolDetailViewModelTests: XCTestCase {
    func testInitFromValidSchool() {
        let school1 = School(name: "School1", schoolID: "SchoolId", borough: "Brooklyn", gradesServed: "9-12", totalStudents: "100")
        let satScore = SATResults(schoolID: "SchoolId", criticalReadingAvgScore: "123", writingAvgScore: "200", mathAvgScore: "300")
        
        let model = SchoolDetailViewModel(school: school1, satScores: satScore)

        XCTAssertEqual(model.schoolName, "School1")
        XCTAssertEqual(model.borough, "Brooklyn")
        XCTAssertEqual(model.gradesServed, "Grades Served: 9-12")
        XCTAssertEqual(model.totalStudents, "Number of Students: 100")
        XCTAssertEqual(model.satScoresDescription, "Average SAT Scores for 2012\nCritical Reading: 123\nMath: 300\nWriting: 200")
    }
    
    func testInitFromInValidSchool() {
        let school1 = School(name: nil, schoolID: nil, borough: nil, gradesServed: nil, totalStudents: nil)
        let satScore = SATResults(schoolID: "SchoolId", criticalReadingAvgScore: "123", writingAvgScore: "200", mathAvgScore: "300")
        
        let model = SchoolDetailViewModel(school: school1, satScores: satScore)

        XCTAssertEqual(model.schoolName, "-")
        XCTAssertEqual(model.borough, "-")
        XCTAssertEqual(model.gradesServed, "Grades Served: -")
    }
}
